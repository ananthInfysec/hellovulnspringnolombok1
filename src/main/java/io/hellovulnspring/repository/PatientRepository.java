package io.hellovulnspring.repository;

import io.hellovulnspring.model.Patient;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PatientRepository extends CrudRepository<Patient, Long> {
}
